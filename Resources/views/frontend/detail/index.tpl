{extends file='parent:frontend/detail/index.tpl'}
{* Main content *}
{block name='frontend_index_content'}
    {include file="parent:frontend/detail/content.tpl"}
{/block}

{block name='frontend_index_header_javascript_jquery_lib'}
    {$smarty.block.parent}
    {block name='frontend_index_header_javascript_jquery_swiper'}
        <script src="{link file="frontend/_public/vendors/js/jquery/jquery.min.js"}"></script>
        <script>
            $(document).ready(function() {

                $(".swiper-container").each(function(i, e) {
                    var swiper = new Swiper(e,{

                        slidesPerView: 1,
                        spaceBetween: 20,
                        autoplay: {
                            delay: 3000,
                        },
                        breakpoints: {

                            640: {
                                slidesPerView: 2,
                                spaceBetween: 30,
                                autoplay: false,
                            },
                            1024: {
                                slidesPerView: 3,
                                spaceBetween: 30,
                                autoplay: false,
                            }
                        },

                        pagination: {
                            el: ".swiper-pagination",
                            clickable: true,
                        },
                        navigation: {
                            nextEl: $(".slider-button-next-" + i)[0],
                            prevEl: $(".slider-button-prev-" + i)[0],
                        },
                    });
                });
                //var swiper = new Swiper(".swiper-container", );

            });
        </script>
    {/block}
{/block}

{block name="frontend_index_header_css_screen"}
    {$smarty.block.parent}
    <link type="text/css" media="all" rel="stylesheet" href="https://unpkg.com/swiper@6.8.4/swiper-bundle.min.css"/>
{/block}

{block name="frontend_detail_index_detail"}
    {if !empty($sArticle.detail_template)}
        {include file="frontend/detail/templates/detail_theme_"|cat:$sArticle.detail_template|cat:".tpl"}
    {else}
        {$smarty.block.parent}
    {/if}
{/block}

{* Tab content container *}
{block name="frontend_detail_index_tab_container"}
    {if empty($sArticle.detail_template)}
        {include file="frontend/detail/content/tab_container.tpl"}
    {/if}
{/block}
