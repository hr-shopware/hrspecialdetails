{block name="frontend_index_content_custom_theme"}

    {namespace name="frontend/detail/HrCustomDetails/customDetails"}
    <div class="hr--custom-detail-container">
        <div class="hr--row img-col-3">
            <div>
                <img src="{link file="frontend/img/Icons-05.png" fullPath}" alt="{s name="cold_press"}{/s}"/>
                <span>{s name="cold_press"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/Icons-06.png" fullPath}" alt="{s name="vegan"}{/s}"/>
                <span>{s name="vegan"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/Icons-11.png" fullPath}" alt="{s name="dermatological"}{/s}"/>
                <span>{s name="dermatological"}{/s}</span>
            </div>
        </div>
        <div class="hr--row video-col">
            <iframe src="https://www.youtube.com/embed/EiM6JGbzFd8" title="PURE Jojobaöl"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
        </div>
        <div class="hr--row text-img-col">
            <div>
                <span class="subtitle">Feuchtigkeitspflege für Haut, Haare und Nägel</span>
                <div class="headline">PURE Bio Pflegeöl</div>
                <div class="text-content">
                    <p>
                        Ob für eine schöne Haut, geschmeidige und glanzvolle Haare oder gepflegte Nägel –
                        PURE Jojobaöl unterstützt auf natürliche Art bei der Körperpflege.
                    </p>
                    <p class="subtitle">
                        Natürlich schön
                    </p>
                    <p>
                        Das PURE Jojobaöl ist frei von zusätzlichen Duftstoffen, Parabenen, Farb- oder
                        Konservierungsstoffen. Durch
                        die native und kaltgepresste Bio-Qualität des Öls bleiben die hochwertigen und
                        naturreinen Inhaltsstoffe wie Vitamin A, E und verschiedene Fettsäuren optimal erhalten.
                    </p>
                    <p>
                        Vitamin A und E sind für die Straffung, Elastizität und Regeneration der Haut förderlich. Sie
                        sorgen dafür, dass eine natürliche Schutzbarriere gegen äußere Einflüsse, wie Bakterien, aufbaut
                        wird. Außerdem verbinden sich die Fettsäuren mit dem hauteigenen Talg und helfen dabei, den
                        Feuchtigkeitsgehalt der Haut zu erhöhen. Das PURE Jojobaöl überzeugt durch die reinigende,
                        feuchtigkeitsspendende und regenerierende Wirkung der natürlichen und reinen Wirkstoffe sowie
                        durch eine tierversuchsfreie Herstellung und eine umweltfreundliche Glasverpackung.
                    </p>
                </div>


            </div>
            <div class="text-img-container">
                <img src="{link file="frontend/img/detail-dunkel.jpg" fullPath}" alt="PURE Bio Pflegeöl"/>
            </div>
        </div>
        <div class="hr--row img-col-4">
            <div>
                <img src="{link file="frontend/img/Icons-12.png" fullPath}" alt="{s name="natural_ingredients"}{/s}"/>
                <span>{s name="natural_ingredients"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/Icons-08.png" fullPath}" alt="{s name="skin_type"}{/s}"/>
                <span>{s name="skin_type"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/Icons-09.png" fullPath}" alt="{s name="no_alcohol"}{/s}"/>
                <span>{s name="no_alcohol"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/Icons-10.png" fullPath}" alt="{s name="fast_absorbed"}{/s}"/>
                <span>{s name="fast_absorbed"}{/s}</span>
            </div>
        </div>
        <div class="hr--row text-col-2">
            <div>
                <span>Feuchtigkeitspflege im Alltag</span>
                <p>
                    Unsere Haut ist täglich verschiedenen Umwelteinflüssen ausgesetzt und häufiges Duschen kann sich
                    negativ auf den Säureschutzmantel auswirken. Unser PURE Jojobaöl spendet der Haut auf natürliche
                    Weise Feuchtigkeit und unterstützt sie dabei, diese zu speichern. Daher ist es insbesondere im
                    Sommer bei der After-Sun-Pflege geeignet. Darüber hinaus bietet es auch im Alltag einen leichten
                    und natürlichen Sonnenschutz mit Lichtschutzfaktor 3.
                </p>
            </div>
            <div>
                <span>Für jeden Hauttyp geeignet</span>
                <p>
                    Das native Bio Jojobaöl ist zur Pflege von trockener, empfindlicher oder fettiger Haut sowie
                    Mischhaut geeignet. Denn es ist eigentlich ein Wachs, das sich ab einer Temperatur von + 8° C
                    verflüssigt, und weist chemische Ähnlichkeit zum Hauttalg des Menschen auf. Diese Eigenschaft macht
                    die Anwendung angenehm. Sie sorgt dafür, dass sich Jojobaöl gut mit der Hautoberfläche verbindet,
                    schnell einzieht und ein angenehmes Hautgefühl hinterlässt – ganz ohne zu fetten.
                </p>
            </div>
        </div>
        <div class="hr--row text-slider">
            <div class="slider-background"></div>
            <div>
                <span class="subtitle">PURE Jojobaöl</span>
                <div class="headline">Allrounder für die Körperpflege</div>
                <div class="text-content half">
                    <p>
                        Aufgrund der verschiedenen Eigenschaften ist auch das Anwendungsgebiet vielseitig. Das PURE
                        Jojobaöl ist ein Allrounder in der Körperpflege. Es hilft einerseits durch seine
                        antibakterielle, reinigende und entzündungshemmende Wirkung bei Hautunreinheiten wie Mitessern,
                        Pickeln und leichter Akne. Neben der Gesichtspflege kann es andererseits ebenfalls als
                        Massageöl, bei der Narbenpflege, Neurodermitis, juckender oder trockener (Kopf-)Haut, der
                        Lippenpflege sowie als Kur bei strapaziertem und glanzlosem Haar angewendet werden.
                    </p>
                </div>
            </div>

            <div class="slider-container">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        {foreach $customDetailSliderData.jojoba as $title => $data}
                            <div class="swiper-slide">
                                <div class="slider-bg theme-1 bg-img-{$data@index}"></div>
                                {include file="frontend/detail/templates/slider/content.tpl" title="{$title}" data=$data}
                            </div>
                        {/foreach}
                    </div>
                </div>
                <div class="swiper-button-prev swiper-button-black slider-button-prev-0"></div>
                <div class="swiper-button-next swiper-button-black slider-button-next-0"></div>
            </div>
        </div>
        <div class="hr--row img-text-col">
            <div class="text-img-container"></div>
            <div>
                <span class="subtitle">Produkt Features</span>
                <div class="headline">Produkt FAQs</div>
                <div class="text-content indent">
                    <p>
                        <span class="subtitle">Wie riecht Jojobaöl?</span><br/>
                        Unser Jojobaöl weist Duftnoten auf, die leicht an Holz erinnern. Da es sich um ein Naturprodukt
                        handelt, ist es möglich, dass diese je nach Ernte variieren. Das hängt meist mit der
                        Natürlichkeit des Wachses zusammen.
                    </p>
                    <p>
                        <span class="subtitle">Wofür verwende ich das Öl am besten?</span><br/>
                        Jojobaöl ist besonders für die Hautpflege geeignet, lässt sich aber auch ideal als Massageöl
                        nutzen. Kaum ein Öl – eigentlich ist es ein flüssiges Wachs – lässt sich vielseitiger anwenden.
                    </p>
                    <p>
                        <span class="subtitle">Muss das Öl vor Sonneneinstrahlung geschützt werden? </span><br/>
                        Für unser PURE Jojobaöl verwenden wir Braunglas, das das Öl vor UV-Strahlung schützt.
                        Denn somit kann die Nährstoffstabilität erhöht werden und die Produkteffektivität verlängert
                        werden. Machen Sie mit und schützen auch Sie das Öl im Alltag vor Sonneneinstrahlung.
                    </p>
                    <p>
                        <span class="subtitle">Was macht das PURE Jojobaöl besonders?</span><br/>
                        Wir legen viel Wert auf eine natürliche und hohe Produktqualität bei unseren Produkten.
                        Das Öl wird schonend durch Kaltpressung extrahiert, um die Vielfalt und den Reichtum an
                        Vitaminen und Fettsäuren zu bewahren.

                    </p>
                </div>
            </div>
        </div>
        <div class="hr--row img-col-4">
            <div>
                <img src="{link file="frontend/img/Icons-07.png" fullPath}" alt="{s name="glasbottle"}{/s}"/>
                <span>{s name="glasbottle"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/Icons-02.png" fullPath}" alt="{s name="animaltests"}{/s}"/>
                <span>{s name="animaltests"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/Icons-03.png" fullPath}" alt="{s name="recycle"}{/s}"/>
                <span>{s name="recycle"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/Icons-04.png" fullPath}" alt="{s name="uvprotected"}{/s}"/>
                <span>{s name="uvprotected"}{/s}</span>
            </div>
        </div>
        <div class="hr--row ratings">
            {if !{config name=VoteDisable}}
                <div class="subtitle">Health Rise Community</div>
                <div class="headline">Kundenbewertungen</div>
                <div>
                    {if $sAction == "ratingAction"}
                        {block name='frontend_detail_comment_error_messages'}
                            {if $sErrorFlag}
                                {if $sErrorFlag['sCaptcha']}
                                    {$file = 'frontend/_includes/messages.tpl'}
                                    {$type = 'error'}
                                    {s  namespace="frontend/detail/comment" name="DetailCommentInfoFillOutCaptcha" assign="content"}{/s}
                                {else}
                                    {$file = 'frontend/_includes/messages.tpl'}
                                    {$type = 'error'}
                                    {s  namespace="frontend/detail/comment" name="DetailCommentInfoFillOutFields" assign="content"}{/s}
                                {/if}
                            {else}
                                {if {config name="OptinVote"} && !{$smarty.get.sConfirmation} && !{$userLoggedIn}}
                                    {$file = 'frontend/_includes/messages.tpl'}
                                    {$type = 'success'}
                                    {s namespace="frontend/detail/comment" name="DetailCommentInfoSuccessOptin" assign="content"}{/s}
                                {else}
                                    {$file = 'frontend/_includes/messages.tpl'}
                                    {$type = 'success'}
                                    {s namespace="frontend/detail/comment" name="DetailCommentInfoSuccess" assign="content"}{/s}
                                {/if}
                            {/if}

                            {include file=$file type=$type content=$content}
                        {/block}
                    {/if}

                    {if $sArticle.sVoteComments}
                        <div class="slider-container">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    {foreach $sArticle.sVoteComments as $vote}
                                        <div class="swiper-slide">
                                            {include file="frontend/detail/comment/entry.tpl" isLast=$vote@last}
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                            <div class="swiper-pagination outside-dots"></div>
                            <div class="swiper-button-prev swiper-button-black slider-button-prev-1"></div>
                            <div class="swiper-button-next swiper-button-black slider-button-next-1"></div>
                        </div>
                    {/if}

                    {* Publish product review *}
                    {block name='frontend_detail_comment_post'}
                        <div class="review--form-container">
                            {include file="frontend/detail/comment/form.tpl"}
                        </div>
                    {/block}
                </div>
            {/if}
        </div>
    </div>
{/block}
