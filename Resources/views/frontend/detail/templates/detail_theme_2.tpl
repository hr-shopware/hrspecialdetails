{block name="frontend_index_content_custom_theme"}

    {namespace name="frontend/detail/HrCustomDetails/jonix"}
    <div class="hr--custom-detail-container">
        <div class="hr--row img-col-5">
            <div>
                <img src="{link file="frontend/img/jonix/viren.png" fullPath}"
                     alt="{s name="icon_1"}{/s}"/>
                <span class="top-space">{s name="icon_1"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/jonix/bakterien.png" fullPath}"
                     alt="{s name="icon_5"}{/s}"/>
                <span class="top-space">{s name="icon_5"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/jonix/gerueche.png" fullPath}"
                     alt="{s name="icon_2"}{/s}"/>
                <span class="top-space">{s name="icon_2"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/jonix/pilze.png" fullPath}"
                     alt="{s name="icon_4"}{/s}"/>
                <span class="top-space">{s name="icon_4"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/jonix/pollen.png" fullPath}"
                     alt="{s name="icon_3"}{/s}"/>
                <span class="top-space">{s name="icon_3"}{/s}</span>
            </div>
        </div>
        <div class="hr--row text-img-col has-rows">
            <div>
                <span class="subtitle">Für Ihr Zuhause und Ihren Arbeitsplatz</span>
                <div class="headline">JONIX CUBE</div>
                <div class="text-content">
                    <p>
                        Fühlen Sie sich wieder sicherer – mittels innovativer nicht-thermischer-Plasma-Technologie
                        (NTP-Technologie) neutralisiert Jonix Cube <strong>99,99 %</strong> der Viren, Bakterien, Schimmelpilze,
                        flüchtigen Schadstoffe und
                        Gerüche in Ihrer Umgebung.
                    </p>
                    <p>
                        Jonix Cube ist ebenfalls gegen SARS-CoV-2 anwendbar. Die <strong>Wirksamkeit der Technologie auf
                            Covid-19</strong>
                        wurde von der Fakultät für Molekulare Medizin der Universität Padua bestätigt. Informieren Sie
                        sich hier näher über den Raumluftreiniger.
                    </p>
                    <a href="{s name="brochure"}{media path='media/pdf/jonix_broschuere.pdf'}{/s}" target="_blank">
                        <button class="btn btn-primary block is--center is--large">Broschüre herunterladen</button>
                    </a>
                </div>
            </div>
            <div class="text-img-container">
                <img src="{link file="frontend/img/jonix/jonix-cube.jpg" fullPath}" alt="PURE Bio Pflegeöl"/>
            </div>
            <div>
                <span class="small-subtitle">Anwendungsbereiche</span>
                <p>
                    Ob im Krankenhaus, Büro, Restaurant, beim Friseur, in der Schule oder in den eigenen vier Wänden –
                    Jonix Cube desinfiziert pathogene Keime und Krankheitserreger auf Flächen und in der Raumluft. Das
                    Luftreinigungsgerät wurde für Räume mit einer Größe <strong>bis zu 85 Quadratmetern
                        Grundfläche</strong> konzipiert.
                    Bei einer größeren Fläche können weitere Geräte bei der Luftreinigung unterstützen.
                </p>
                <p>
                    Oftmals ist die Luft in geschlossenen Räumen – trotz Lüften – stärker kontaminiert als man denkt.
                    Abhängig von der Raumgröße und der Personenanzahl isoliert <strong>Jonix Cube</strong> nicht nur
                    flüchtige
                    Schadstoffe durch <strong>Feinstaub, Farben und Reinigungsmittel</strong>, sondern wirkt auch
                    Kochdämpfen oder der
                    <strong>Gefahr von Bakterien und Viren</strong> entgegen. Der Luftreiniger kann ohne Unterbrechungen auch
                    über einen
                    längeren Zeitraum und mit Personen im Raum eingesetzt werden. Mit dem Jonix Cube können Sie
                    <strong>beruhigter in Innenräumen durchatmen</strong> und sich wohler fühlen.
                </p>
            </div>
            <div>
                <span class="small-subtitle">Ihre Vorteile</span>
                <p>
                    Jonix Cube verbindet eine hohe Wirksamkeit für Ihre Gesundheit mit leicht verständlicher Anwendung
                    und geringem Energieverbrauch. Das Gerät zur Dekontamination lässt sich <strong>einfach
                        installieren</strong>,
                    eigenständig durch Display-Anweisungen warten und ermöglicht eine hohe Reinigungsleistung bei einem
                    <strong>Energieverbrauch von lediglich 10 Watt</strong>.
                </p>
                <p>
                    Indem Jonix Cube kontinuierlich die Raumluft desinfiziert, verringert er das Infektionsrisiko durch
                    luftübertragende Krankheiten, wirkt Atembeschwerden entgegen und eignet sich daher gut für
                    empfindlichere Personengruppen wie <strong>Kinder, ältere Menschen, Asthmatikern oder Allergiker</strong>.
                </p>
            </div>
        </div>
        <div class="hr--row text-img-col">
            <div class="extra subtitle">Technologie und Zertifizierung</div>
            <div class="extra headline">Für Ihre Gesundheit ist gesorgt</div>
            <div>
                <div class="text-content">
                    <p>
                        Die innovative <strong>Kalt-Plasma-Technologie</strong> (NTP-Technologie) von Jonix Cube sorgt für eine frische
                        und gereinigte Luft. Das Verfahren ist eine weiterentwickelte Form der Ionisierung und greift
                        auf die dekontaminierenden Attribute der Luft zurück, wenn sie durch kontrollierte Energie
                        aktiviert wird. Der Raumluftreiniger gibt – <strong>ohne den Einsatz von Chemikalien und ohne
                            Nebenwirkungen</strong> –
                        Ionen in die Umgebung ab, welche sich mit einer Vielzahl von Schadstoffen wie Bakterien und
                        Viren verbinden, diese deaktivieren und rückstandslos abbauen.
                    </p>
                    <p>
                        Sobald neue Personen einen gereinigten Raum betreten, wird mit diesem Verfahren eine mögliche
                        Ansteckung durch aktive Aerosole weitgehend ausgeschlossen. Die erprobte NTP-Technologie trägt zur
                        <strong>Hygiene und Gesundheit</strong> bei. Daher ist es nicht verwunderlich, dass Jonix Cube
                        bereits seit
                        Jahren in <strong>Krankenhäusern und Operationssälen</strong> Anwendung findet und auch in
                        anderen Räumlichkeiten
                        ideale Bedingungen für das <strong>Wohlbefinden der Menschen</strong> schafft.
                    </p>
                </div>
            </div>
            <div class="text-img-container">
                <img src="{link file="frontend/img/jonix/Technologie_und_Z.jpg" fullPath}" alt="PURE Bio Pflegeöl"/>
            </div>
        </div>
        <div class="hr--row text-slider">
            <div class="slider-background"></div>
            <div class="slider-container jonix-seals">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        {foreach $customDetailSliderData.jonix as $title => $data}
                            <div class="swiper-slide">
                                <div class="slider-bg theme-2 bg-img-{$data@index}"></div>
                                {include file="frontend/detail/templates/slider/content.tpl" title="{$title}" data=$data style="jonix"}
                            </div>
                        {/foreach}
                    </div>
                </div>
                <div class="swiper-button-prev swiper-button-black slider-button-prev-0"></div>
                <div class="swiper-button-next swiper-button-black slider-button-next-0"></div>
            </div>
        </div>
        <div class="hr--row img-col-5">
            <div>
                <img src="{link file="frontend/img/jonix/schnelle_l.png" fullPath}"
                     alt="{s name="icon_10"}{/s}"/>
                <span class="top-space">{s name="icon_10"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/jonix/kompakt.png" fullPath}"
                     alt="{s name="icon_7"}{/s}"/>
                <span class="top-space">{s name="icon_7"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/jonix/geraeusche.png" fullPath}"
                     alt="{s name="icon_8"}{/s}"/>
                <span class="top-space">{s name="icon_8"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/jonix/eliminierte_g.png" fullPath}"
                     alt="{s name="icon_9"}{/s}"/>
                <span class="top-space">{s name="icon_9"}{/s}</span>
            </div>
            <div>
                <img src="{link file="frontend/img/jonix/luftzug.png" fullPath}"
                     alt="{s name="icon_6"}{/s}"/>
                <span class="top-space">{s name="icon_6"}{/s}</span>
            </div>
        </div>
        <div class="hr--row img-text-col">
            <div class="text-img-container jonix-faq"></div>
            <div>
                <span class="subtitle">Haben Sie Fragen?</span>
                <div class="headline">Produkt FAQs</div>
                <div class="text-content indent">
                    <p>
                        <span class="subtitle">Wie laut ist das Jonix Cube?</span><br/>
                        Bei absoluter Stille kann die Lautstärke des Raumluftreinigers zunächst auffallen. Der gleichmäßige Ton
                        des Gerätes ist jedoch selten dauerhaft unangenehm. Insbesondere bei Hintergrund- oder
                        Nebengeräuschen wirkt Jonix Cube wenig störend. Im direkten Vergleich zu HEPA-Filtergeräten sind
                        Luftreiniger mit Ionen- oder Plasmatechnologie in der Regel geräuscharmer.
                    </p>
                    <p>
                        <span class="subtitle">Wo muss das Gerät im Raum platziert werden?</span><br/>
                        Mit einer Größe von 23,8 x 23,8 x 26 cm und einem Gewicht von circa 4 kg kann Jonix Cube an
                        vielen Orten im Raum platziert werden. Die NTP-Technologie verbreitet die Ionen
                        abhängig von Personen- und Raumgröße in Innenräumen in alle Richtungen aus.
                    </p>
                    <p>
                        <span class="subtitle">Worin liegt der Unterschied zu einem Hepa-Filter?</span><br/>
                        Durch die Verunreinigung des HEPA-Filters muss dieser regelmäßig durch Fachpersonal ausgetauscht
                        werden, wohingegen der Jonix Cube eigenständig und ohne Schutzkleidung gereinigt und gewartet
                        werden kann. Die Gefahr vor einer Übertragung von Viren oder Bakterien bei dem Austauschen der
                        Ersatzteile ist – im Gegensatz zu HEPA-Filtergeräten – durch die NTP-Technologie nicht
                        gegeben. Darüber hinaus erzeugt ein HEPA-Filtergerät einen Luftzug, der je nach Platzierung des
                        Gerätes im Raum, unangenehm sein kann. Auch auf die Anordnung im Raum muss geachtet werden,
                        welches bei der Anwendung des Jonix Cube weniger eine Rolle spielt, da sich die Ionen
                        gleichmäßig im Raum verteilen und neue Bakterien und Viren kontinuierlich deaktivieren. Bei
                        HEPA-Filtergeräten werden hingegen lediglich die Luftbereiche gereinigt, die in das Gerät
                        gelangen, welches eine Luftreinigung des Raumes bei suboptimaler Platzierung des Gerätes
                        zeitversetzt und unvollständig erfolgen lässt.
                    </p>
                </div>
            </div>
        </div>
        <div class="hr--row ratings">
            {if !{config name=VoteDisable}}
                <div class="subtitle">Health Rise Community</div>
                <div class="headline">Kundenbewertungen</div>
                <div>
                    {if $sAction == "ratingAction"}
                        {block name='frontend_detail_comment_error_messages'}
                            {if $sErrorFlag}
                                {if $sErrorFlag['sCaptcha']}
                                    {$file = 'frontend/_includes/messages.tpl'}
                                    {$type = 'error'}
                                    {s  namespace="frontend/detail/comment" name="DetailCommentInfoFillOutCaptcha" assign="content"}{/s}
                                {else}
                                    {$file = 'frontend/_includes/messages.tpl'}
                                    {$type = 'error'}
                                    {s  namespace="frontend/detail/comment" name="DetailCommentInfoFillOutFields" assign="content"}{/s}
                                {/if}
                            {else}
                                {if {config name="OptinVote"} && !{$smarty.get.sConfirmation} && !{$userLoggedIn}}
                                    {$file = 'frontend/_includes/messages.tpl'}
                                    {$type = 'success'}
                                    {s namespace="frontend/detail/comment" name="DetailCommentInfoSuccessOptin" assign="content"}{/s}
                                {else}
                                    {$file = 'frontend/_includes/messages.tpl'}
                                    {$type = 'success'}
                                    {s namespace="frontend/detail/comment" name="DetailCommentInfoSuccess" assign="content"}{/s}
                                {/if}
                            {/if}

                            {include file=$file type=$type content=$content}
                        {/block}
                    {/if}

                    {if $sArticle.sVoteComments}
                        <div class="slider-container">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    {foreach $sArticle.sVoteComments as $vote}
                                        <div class="swiper-slide">
                                            {include file="frontend/detail/comment/entry.tpl" isLast=$vote@last}
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                            <div class="swiper-pagination outside-dots"></div>
                            <div class="swiper-button-prev swiper-button-black slider-button-prev-1"></div>
                            <div class="swiper-button-next swiper-button-black slider-button-next-1"></div>
                        </div>
                    {/if}

                    {* Publish product review *}
                    {block name='frontend_detail_comment_post'}
                        <div class="review--form-container">
                            {include file="frontend/detail/comment/form.tpl"}
                        </div>
                    {/block}
                </div>
            {/if}
        </div>
        <div class="hr--row seals">

            <img src="{link file="frontend/img/jonix/seals/HR.png" fullPath}" class="seal-image"
                 alt=""/>

            <img src="{link file="frontend/img/jonix/seals/biosafe.png" fullPath}" class="seal-image"
                 alt=""/>

            <img src="{link file="frontend/img/jonix/seals/Virus.png" fullPath}" class="seal-image"
                 alt=""/>

            <img src="{link file="frontend/img/jonix/seals/ongreening.png" fullPath}" class="seal-image"
                 alt=""/>

            <img src="{link file="frontend/img/jonix/seals/ce.png" fullPath}" class="seal-image"
                 alt=""/>

            <img src="{link file="frontend/img/jonix/seals/eu.png" fullPath}" class="seal-image"
                 alt=""/>

            <img src="{link file="frontend/img/jonix/seals/tuev.png" fullPath}" class="seal-image"
                 alt=""/>
        </div>
    </div>
{/block}
