<div class="slide-content">
    {if $style === "jonix"}
        {if $data.0|strpos:"img "!== false}
            <img src="{link file=$data.0|substr:4 fullPath}" class="slider-seal-img" alt=""/>
            <div>{$title}</div>
        {/if}
    {else}
        <div>{$title}</div>
    {/if}

    <ul>
        {foreach $data as $key => $value}
            {if is_array($value)}
                <li>{$key}
                    <ul>
                        {foreach $value as $v}
                            <li>{$v}</li>
                        {/foreach}
                    </ul>
                </li>
            {else}
                {if $value|strpos:"img " === false}
                    <li>{$value}</li>
                {/if}
            {/if}
        {/foreach}
    </ul>
</div>
