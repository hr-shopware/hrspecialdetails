<?php
namespace HrSpecialDetails;

use Shopware\Bundle\AttributeBundle\Service\CrudServiceInterface;
use Shopware\Bundle\AttributeBundle\Service\TableMappingInterface;
use Shopware\Bundle\AttributeBundle\Service\TypeMapping;
use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;
use Shopware\Components\Plugin\Context\UpdateContext;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class HrSpecialDetails extends Plugin
{
    private const ARTICLE_ATTRIBUTES = 's_articles_attributes';

    protected $container;

    public function build(ContainerBuilder $container): void
    {
        $this->container = $container;
        parent::build($container);
    }

    public function install(InstallContext $context): void
    {
        $this->runConfiguration();
    }

    public function update(UpdateContext $context): void
    {
        $currentVersion = $context->getCurrentVersion();
        $this->runConfiguration($currentVersion);
        $context->scheduleClearCache(InstallContext::CACHE_LIST_FRONTEND);
    }

    public function uninstall(UninstallContext $context): void
    {
        $this->deleteColumnsIfExists(['use_custom_theme', 'detail_template']);
        $this->reGenerateAttributes();
    }

    /**
     * @param array $columns
     */
    private function deleteColumnsIfExists(array $columns = []): void
    {
        if (!empty($columns)) {
            /** @var CrudServiceInterface $crudService */
            $crudService = $this->container->get('shopware_attribute.crud_service');
            /** @var TableMappingInterface $tableMapping */
            $tableMapping = $this->container->get('shopware_attribute.table_mapping');

            foreach ($columns as $column) {
                if ($tableMapping->isTableColumn(self::ARTICLE_ATTRIBUTES, $column)) {
                    $crudService->delete(
                        self::ARTICLE_ATTRIBUTES,
                        $column
                    );
                }
            }
        }
    }

    private function reGenerateAttributes(): void
    {
        $mm = $this->container->get('models');
        $mm->generateAttributeModels(
            [
                self::ARTICLE_ATTRIBUTES,
            ]
        );
    }

    private function runConfiguration(string $existingVersion = '1.0.0'): void
    {
        $crudService  = $this->container->get('shopware_attribute.crud_service');

        switch (true) {

            case version_compare($existingVersion, '1.9.1', '<='):
                $crudService->update(
                    self::ARTICLE_ATTRIBUTES,
                    'detail_template',
                    TypeMapping::TYPE_COMBOBOX,
                    [
                        'displayInBackend' => true,
                        'label'            => 'Detail Template',
                        'translatable'     => false,
                        'custom'           => false,
                        'position'         => 101,
                        'arrayStore'       => [
                            ['key' => '0', 'value' => 'Standard'],
                            ['key' => '1', 'value' => 'Jojoba'],
                            ['key' => '2', 'value' => 'Jonix'],
                        ],
                    ],
                    null,
                    true,
                    '0'
                );

            //case version_compare($existingVersion, '1.1.26', '<='):*/
        }
        $this->reGenerateAttributes();
    }
}
