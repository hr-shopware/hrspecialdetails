<?php

namespace HrSpecialDetails\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Event_EventArgs;
use Enlight_Template_Manager;
use Shopware_Controllers_Frontend_Detail;

class TemplateSubscriber implements SubscriberInterface
{
    /**
     * @var Enlight_Template_Manager
     */
    private Enlight_Template_Manager $templateManager;

    /**
     * @var string
     */
    private string $viewDirectory;

    /**
     * templateSubscriber constructor.
     *
     * @param Enlight_Template_Manager $templateManager
     * @param                          $viewDirectory
     */
    public function __construct(Enlight_Template_Manager $templateManager, $viewDirectory)
    {
        $this->templateManager = $templateManager;
        $this->viewDirectory   = $viewDirectory;
    }

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PreDispatch_Frontend_Detail' => 'onPreDispatchFrontendDetail'
        ];
    }

    public function onPreDispatchFrontendDetail(Enlight_Event_EventArgs $args)
    {
        /** @var Shopware_Controllers_Frontend_Detail $subject */
        $subject = $args->getSubject();
        $view    = $subject->View();
        $view->assign('imageDir', $this->viewDirectory . '/_public/src/img');
        $view->assign('customDetailSliderData', $this->properties());
        $this->templateManager->addTemplateDir($this->viewDirectory);
    }

    /**
     * @return array
     */
    private function properties(): array
    {
        return [
            'jojoba' => [
                'Merkmale'              => [
                    'nicht fettend',
                    'antibakteriell',
                    'wirkt bei Hautunreinheiten',
                    'reguliert die Talgproduktion',
                    'frei von Farb- und Duftstoffen',
                    'frei von Konservierungsstoffen',
                ],
                'Nährstoffe'            => [
                    'Vitamin A',
                    'Vitamin E',
                    'verschiedene Fettsäuren',
                ],
                'Hauttypen'             => [
                    'Mischhaut',
                    'fettige Haut',
                    'trockene Haut',
                    'empfindliche Haut',
                ],
                'Gesichtspflege'        => [
                    'intensive Pflege für Lippen',
                    'Abschminken von Make-Up',
                    'lokale Behandlung von Hautunreinheiten',
                    'Gesichtsreinigung und Feuchtigkeitspflege',
                ],
                'Körperpflege'          => [
                    'Massageöl für den Körper',
                    'schützende After-Sun-Pflege',
                    'Basisöl in der Aromatherapie',
                    'Reinigung empfindlicher Hautpartien',
                    'Feuchtigkeitspflege für jeden Hauttyp',
                    'Anwendung bei Narben oder Neurodermitis',
                    'Straffung, Elastizität und Regeneration der Haut',
                ],
                'Haar- und Nagelpflege' => [
                    'intensive Pflege für Nägel',
                    'natürliche Kur für glänzendes und geschmeidiges Haar',
                    'Wirkung bei juckender, trockener und schuppiger Kopfhaut',
                ],
            ],
            'jonix'  => [
                'Wirksamkeit der Technologie auf Covid-19'           => [
                    'img frontend/img/jonix/slider-bg/virus.png',
                    'geprüft und bestätigt von der Fakultät für Molekulare Medizin der Universität Padua',
                ],
                'TÜV PROFiCERT – Produkt'                            => [
                    'img frontend/img/jonix/slider-bg/tuev_profi.png',
                    'validiert die <strong>Wahrhaftigkeit</strong> der beschriebenen Angaben in den wissenschaftlichen Dossiers und Produktkatalogen',
                ],
                'CE-Kennzeichnung'                                   => [
                    'img frontend/img/jonix/slider-bg/ce.png',
                    'gibt Aufschluss über die Einhaltung von <strong>Sicherheitsanforderungen</strong> der geltenden EG-Richtlinien',
                ],
                'Bio-Safe®-Zertifizierung'                           => [
                    'img frontend/img/jonix/slider-bg/biosafe.png',
                    'bescheinigt die Wirksamkeit von Produkten bei der Verringerung von Schadstoffen',
                    'validiert die <strong>Umweltgesundheit</strong>',
                    'zertifiziert den ausgezeichneten Wohnkomfort in Innenräumen',
                ],
                'Ongreening® e ProductMAP®'                          => [
                    'img frontend/img/jonix/slider-bg/ongreening.png',
                    'deutet auf die <strong>nachhaltigkeitsorientierten Praktiken</strong> und Materialdaten des Produktes hin',

                ],
                'Siegel für hervorragende Leistungen - Horizon 2020' => [
                    'img frontend/img/jonix/slider-bg/EUquality.png',
                    'honoriert den hohen <strong>Qualitätsstandard</strong> des Projektvorschlags „JONIXAirPlasma“ seitens der Europäischen Kommission im Rahmen des EU-Rahmenprogramms für Forschung und Innovation 2014-2020',
                ],
                'Prüfung durch Health Rise'                          => [
                    'img frontend/img/jonix/slider-bg/HR_siegel.jpg',
                    'weist auf die Prüfung des Gerätes nach den <strong>Qualitäts- und Sicherheitsstandards</strong> des Gesundheitsportals Health Rise hin',
                ],
            ],
        ];
    }
}
